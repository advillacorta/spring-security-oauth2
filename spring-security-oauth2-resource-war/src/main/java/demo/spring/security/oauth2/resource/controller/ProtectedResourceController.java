package demo.spring.security.oauth2.resource.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/protected")
public class ProtectedResourceController 
{
	@RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Resource getResource()
	{
		return new Resource("Protected resource", "This is a protected resource");
	}
	
	public class Resource
	{
		private String name;
		private String details;
		
		public Resource(String name, String details)
		{
			this.name = name;
			this.details = details;
		}
		
		public String getName() 
		{
			return name;
		}
		
		public void setName(String name) 
		{
			this.name = name;
		}
		
		public String getDetails() 
		{
			return details;
		}
		
		public void setDetails(String details) 
		{
			this.details = details;
		}
	}
}