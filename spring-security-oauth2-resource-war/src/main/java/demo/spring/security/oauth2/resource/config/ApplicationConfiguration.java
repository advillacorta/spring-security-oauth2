package demo.spring.security.oauth2.resource.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@PropertySource("classpath:resource-server.properties")
@ComponentScan(basePackages = { "demo.spring.security.oauth2.resource" })
public class ApplicationConfiguration 
{
	
}
