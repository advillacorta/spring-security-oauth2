package demo.spring.security.oauth2.resource.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

/**
 * Clase encargada de configurar un servicio de recursos.
 * 
 * @author Synopsis S.A
 *
 */
@Configuration
@EnableWebSecurity
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter 
{
	@Autowired
	private Environment env;
	
	@Override
	public void configure(HttpSecurity http) throws Exception 
	{
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
		.anyRequest().authenticated();
	}
	
	/**
	 * M&eacute;todo encargado de configurar el servidor de recursos. En este caso se configura unicamente el servicio de validaci&oacute;n de tokens JWT.
	 * 
	 * @see org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter#configure(org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer)
	 */
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception 
	{
		resources.tokenServices(tokenService());
	}
	
	/**
	 * M&eacute;todo encargado de configurar el servicio remoto de validaci&oacute;n de token JWT.
	 * Este apunta al servidor de autorizaci&oacute;n previamente definido.
	 * 
	 * @return
	 */
	@Primary
	@Bean
	public RemoteTokenServices tokenService() 
	{
		final RemoteTokenServices tokenService = new RemoteTokenServices();
		tokenService.setCheckTokenEndpointUrl(env.getProperty("checktoken.endpoint.url"));
		tokenService.setClientId(env.getProperty("checktoken.client.id"));
		tokenService.setClientSecret(env.getProperty("checktoken.client.secret"));
		return tokenService;
	}
}
