package demo.spring.security.oauth2.auth.token;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

/**
 * Clase encargada de personalizar la informaci&oacute;n a ser enviada como
 * parta del payload del token JWT.
 * 
 * @author Synopsis S.A
 *
 */
public class CustomTokenEnhancer implements TokenEnhancer {
    @SuppressWarnings("unchecked")
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        // Setting additional information for token
        Map<String, Object> additionalInformation = new HashMap<String, Object>();
        additionalInformation.putAll((Map<String, String>) authentication.getUserAuthentication().getDetails());
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInformation);
        return accessToken;
    }
}