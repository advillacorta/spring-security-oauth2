package demo.spring.security.oauth2.auth.security;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.approval.UserApprovalHandler;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenEndpointFilter;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import demo.spring.security.oauth2.auth.token.CustomTokenEnhancer;

/**
 * Clase encargada de definir un servidor de autorizaci&oacute;n para la administraci&oacute;n
 * de tokens de autenticaci&oacute;n para los diferentes clientes.
 * 
 * @author Synopsis S.A
 *
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServer extends AuthorizationServerConfigurerAdapter 
{
	@Autowired
	@Qualifier("authenticationManagerBean")
    private AuthenticationManager authenticationManager;
	
    @Autowired
    private UserApprovalHandler userApprovalHandler;
	
    /**
     * M&eacute;todo encargado de realizar la configuraci&oacute;n de los clientes autorizados.
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception 
    {
        clients.inMemory()
        	.withClient("test_app")
        	.scopes("self")
        	.authorizedGrantTypes("password");
        	
        	// Token will expire in 6 minutes
        	//.accessTokenValiditySeconds(360);
    }
    
	/**
	 * M&eacute;todo encargado de realizar la configuraci&oacute;n de los endpoints del serviro de autorizaci&oacute;n.
	 */
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception 
	{
		// Token enhancer
		TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
		tokenEnhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer(), jwtTokenConverter()));
		
		// Setup
	    endpoints
	    // Endpoints url customization
	    .pathMapping("/oauth/confirm_access", "/spring/demo/oauth/confirm_access")
	    .pathMapping("/oauth/check_token", "/spring/demo/oauth/check_token")
	    .pathMapping("/oauth/token_key", "/spring/demo/oauth/token_key")
	    .pathMapping("/oauth/authorize", "/spring/demo/oauth/authorize")
	    .pathMapping("/oauth/token", "/spring/demo/oauth/token")
	    .pathMapping("/oauth/error", "/spring/demo/oauth/error")
	    .tokenStore(tokenStore())
	    .tokenEnhancer(tokenEnhancerChain)
	    .userApprovalHandler(userApprovalHandler)
	    .authenticationManager(authenticationManager);
	}
    
    /**
     * M&eacute;todo encargado de realizar la configuraci&oacute;n de seguridad del servidor de autorizaci&oacute;n.
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer)throws Exception 
    {
        oauthServer
        .allowFormAuthenticationForClients()
		.tokenKeyAccess("permitAll()")
		.checkTokenAccess("isAuthenticated()")
		.addTokenEndpointAuthenticationFilter(checkTokenEndpointFilter());
    }
    
    /**
     * M&eacute;todo encargado de crear un token store del tipo JWT.
     * 
     * @return
     */
    @Bean
    public TokenStore tokenStore() 
    {
        return new JwtTokenStore(jwtTokenConverter());
    }
    
	/**
	 * M&eacute;todo encargado de crear un convertidor de tokens del tipo JWT.
	 * 
	 * @return
	 */
	@Bean
    public JwtAccessTokenConverter jwtTokenConverter() 
	{
		// Reading RSA Key pair for token signing
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(new ClassPathResource("jwt.jks"), "mySecretKey".toCharArray());
        converter.setKeyPair(keyStoreKeyFactory.getKeyPair("jwt"));
        return converter;
    }
	
	/**
	 * M&eacute;todo encargado de crear un filtro para el endpoint utilizado para la validaci&oacute;n del token JWT generado.
	 * 
	 * @return
	 */
	public ClientCredentialsTokenEndpointFilter checkTokenEndpointFilter() 
	{
		// This enables the check token endpoint to be use for remote token validation
	    ClientCredentialsTokenEndpointFilter filter = new ClientCredentialsTokenEndpointFilter("/oauth/check_token");
	    filter.setAuthenticationManager(authenticationManager);
	    filter.setAllowOnlyPost(true);
	    return filter;
	}
	
    @Bean
    public TokenEnhancer tokenEnhancer()
    {
    	return new CustomTokenEnhancer();
    }
}