package demo.spring.security.oauth2.auth.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { "demo.spring.security.oauth2.auth" })
public class ApplicationConfiguration 
{

}
